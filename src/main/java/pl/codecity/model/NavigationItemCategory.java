package pl.codecity.model;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToOne;

@Entity
@DynamicInsert
@DynamicUpdate
@DiscriminatorValue("category")
@SuppressWarnings("serial")
public class NavigationItemCategory extends NavigationItem {

    @OneToOne
    private Category category;

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    @Override
    public String print() {
        return getCategory().getName();
    }
}

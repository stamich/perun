package pl.codecity.model;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.search.annotations.Analyze;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.IndexedEmbedded;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@DynamicInsert
@DynamicUpdate
@NamedEntityGraphs({@NamedEntityGraph(name = Blog.DEEP_GRAPH_NAME, attributeNodes = {@NamedAttributeNode("languages")})})
@Table(name = "BLOG")
@SuppressWarnings("serial")
public class Blog extends AbstractDomainObject<Integer> {

    public static final Integer DEFAULT_ID = 1;

    public static final String DEEP_GRAPH_NAME = "BLOG_DEEP_GRAPH";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Field(analyze = Analyze.NO)
    @Column(length = 200, nullable = false, unique = true)
    private String code;

    @Field
    @Column(length = 3, nullable = false)
    private String defaultLanguage;

    @IndexedEmbedded(includeEmbeddedObjectId = true)
    @OneToMany(mappedBy = "blog", cascade = CascadeType.ALL)
    private Set<BlogLanguage> languages = new HashSet<>();

    // Abstract methods

    @Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String print() {
        return null;
    }

    // Getters and setters

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDefaultLanguage() {
        return defaultLanguage;
    }

    public void setDefaultLanguage(String defaultLanguage) {
        this.defaultLanguage = defaultLanguage;
    }

    public Set<BlogLanguage> getLanguages() {
        return languages;
    }

    public void setLanguages(Set<BlogLanguage> languages) {
        this.languages = languages;
    }

    // Other methods

    public BlogLanguage getLanguage(String language) {
        for (BlogLanguage blogLanguage : getLanguages()) {
            if (blogLanguage.getLanguage().equals(language)) {
                return blogLanguage;
            }
        }
        return null;
    }

    public String getTitle() {
        return getTitle(getDefaultLanguage());
    }

    public String getTitle(String language) {
        return getLanguage(language).getTitle();
    }

    public boolean isMultiLanguage() {
        return (getLanguages().size() > 1);
    }
}
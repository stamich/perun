package pl.codecity.model;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@DynamicInsert
@DynamicUpdate
@Table(name = "password_reset_token")
@Inheritance(strategy = InheritanceType.JOINED)
public class PasswordResetToken extends AbstractDomainObject<String> {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid2")
    @Column(length = 50)
    private String token;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    private User user;

    @Column(length = 200, nullable = false)
    private String email;

    @Column(nullable = false)
    private LocalDateTime expiredAt;

    // Abstract methods

    @Override
    public String getId() {
        return getToken();
    }

    @Override
    public String print() {
        return getEmail() + " " + getToken();
    }

    // Getters and setters

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LocalDateTime getExpiredAt() {
        return expiredAt;
    }

    public void setExpiredAt(LocalDateTime expiredAt) {
        this.expiredAt = expiredAt;
    }
}

package pl.codecity.model;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.search.annotations.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Indexed
@DynamicInsert
@DynamicUpdate
@Table(name = "PAGE")
@Analyzer(definition = "synonyms")
@NamedEntityGraphs({@NamedEntityGraph(name = Page.SHALLOW_GRAPH_NAME, attributeNodes = {
        @NamedAttributeNode("cover"),
        @NamedAttributeNode("author"),
        @NamedAttributeNode("parent"),
        @NamedAttributeNode("children"),
        @NamedAttributeNode("categories"),
        @NamedAttributeNode("tags")}),
        @NamedEntityGraph(name = Page.DEEP_GRAPH_NAME, attributeNodes = {
                @NamedAttributeNode("cover"),
                @NamedAttributeNode("author"),
                @NamedAttributeNode("parent"),
                @NamedAttributeNode("children"),
                @NamedAttributeNode("categories"),
                @NamedAttributeNode("tags"),
                @NamedAttributeNode("relatedToPosts"),
                @NamedAttributeNode(value = "customFieldValues", subgraph = "customFieldValue")}, subgraphs =  {
                @NamedSubgraph(name = "customFieldValue", attributeNodes = {@NamedAttributeNode("customField")})})})
@SuppressWarnings("serial")
public class Page extends Post implements Comparable<Page>{

    public static final String SHALLOW_GRAPH_NAME = "PAGE_SHALLOW_GRAPH";
    public static final String DEEP_GRAPH_NAME = "PAGE_DEEP_GRAPH";

    @Column(nullable = false)
    @SortableField(forField = "sortLft")
    @Fields({@Field, @Field(name = "sortLft", analyze = Analyze.NO, index = org.hibernate.search.annotations.Index.NO)})
    private Integer lft;

    @Field
    @Column(nullable = false)
    private Integer rgt;

    @ManyToOne
    private Page parent;

    @OneToMany(mappedBy = "parent", cascade = CascadeType.ALL)
    private List<Page> child;

    // Getters and setters

    public Integer getLft() {
        return lft;
    }

    public void setLft(Integer lft) {
        this.lft = lft;
    }

    public Integer getRgt() {
        return rgt;
    }

    public void setRgt(Integer rgt) {
        this.rgt = rgt;
    }

    public Page getParent() {
        return parent;
    }

    public void setParent(Page parent) {
        this.parent = parent;
    }

    public List<Page> getChild() {
        return child;
    }

    public void setChild(List<Page> child) {
        this.child = child;
    }

    // Other methods

    public int compareTo(Page page) {
        int lftDiff = getLft() - page.getLft();
        if (lftDiff != 0) {
            return lftDiff;
        }
        return (int) (page.getId() - getId());
    }
}

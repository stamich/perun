package pl.codecity.model;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.SortNatural;
import org.hibernate.search.annotations.*;
import org.hibernate.search.annotations.Index;

import javax.persistence.*;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

@Entity
@DynamicInsert
@DynamicUpdate
@Indexed
@NamedEntityGraphs({
        @NamedEntityGraph(name = Category.SHALLOW_GRAPH_NAME, attributeNodes = {@NamedAttributeNode("parent"), @NamedAttributeNode("children")}),
        @NamedEntityGraph(name = Category.DEEP_GRAPH_NAME, attributeNodes = {@NamedAttributeNode("parent"), @NamedAttributeNode("children")})})
@Table(name = "CATEGORY", uniqueConstraints = @UniqueConstraint(columnNames = {"code", "language"}))
@SuppressWarnings("serial")
public class Category extends AbstractDomainObject<Integer> implements Comparable<Category>{

    public static final String SHALLOW_GRAPH_NAME = "CATEGORY_SHALLOW_GRAPH";
    public static final String DEEP_GRAPH_NAME = "CATEGORY_DEEP_GRAPH";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Field(name = "sortId", analyze = Analyze.NO, index = Index.NO)
    @SortableField(forField = "sortId")
    private Integer id;

    @Column(length = 200, nullable = false)
    @Field(analyze = Analyze.NO)
    private String code;

    @Column(length = 3, nullable = false)
    @Field
    private String language;

    @Column(length = 200, nullable = false)
    @Fields({@Field, @Field(name = "sortName", analyze = Analyze.NO, index = org.hibernate.search.annotations.Index.NO)})
    @SortableField(forField = "sortName")
    private String name;

    @Lob
    @Field
    private String description;

    @Column(nullable = false)
    @Field
    private int lft;

    @Column(nullable = false)
    @Field
    private int rgt;

    @ManyToOne
    private Category parent;

    @OneToMany(mappedBy = "parent", cascade = CascadeType.ALL)
    private List<Category> children;

    @ManyToMany
    @JoinTable(
            name = "post_category",
            joinColumns = {@JoinColumn(name = "category_id")},
            inverseJoinColumns = @JoinColumn(name = "post_id", referencedColumnName = "id"))
    @SortNatural
    private SortedSet<Post> posts = new TreeSet<>();

    // Abstract methods

    @Override
    public Integer getId() {
        return null;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String print() {
        return getName();
    }

    @Override
    public int compareTo(Category category) {
        int lftDiff = getLft() - category.getLft();
        if (lftDiff != 0) {
            return lftDiff;
        }
        return (int) (category.getId() - getId());
    }

    // Getters and setters

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getLft() {
        return lft;
    }

    public void setLft(int lft) {
        this.lft = lft;
    }

    public int getRgt() {
        return rgt;
    }

    public void setRgt(int rgt) {
        this.rgt = rgt;
    }

    public Category getParent() {
        return parent;
    }

    public void setParent(Category parent) {
        this.parent = parent;
    }

    public List<Category> getChildren() {
        return children;
    }

    public void setChildren(List<Category> children) {
        this.children = children;
    }

    public SortedSet<Post> getPosts() {
        return posts;
    }

    public void setPosts(SortedSet<Post> posts) {
        this.posts = posts;
    }

    // Object class methods

    @Override
    public String toString() {
        return getName();
    }
}

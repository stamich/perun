package pl.codecity.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.codecity.model.Media;

@Repository
public interface MediaRepository extends JpaRepository<Media, String> {

}

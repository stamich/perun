package pl.codecity.repository;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.stereotype.Repository;
import pl.codecity.model.Blog;

import javax.persistence.LockModeType;

@Repository
public interface BlogRepository extends JpaRepository<Blog, Integer> {

    @EntityGraph(value = Blog.DEEP_GRAPH_NAME, type = EntityGraph.EntityGraphType.FETCH)
    Blog findOneById(Integer id);

    @Lock(LockModeType.PESSIMISTIC_WRITE)
    @EntityGraph(value = Blog.DEEP_GRAPH_NAME, type = EntityGraph.EntityGraphType.FETCH)
    Blog findOneForUpdateById(Integer id);
}

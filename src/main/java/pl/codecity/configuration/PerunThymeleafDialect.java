package pl.codecity.configuration;

import org.thymeleaf.dialect.AbstractDialect;
import org.thymeleaf.dialect.IExpressionObjectDialect;
import org.thymeleaf.expression.IExpressionObjectFactory;

public class PerunThymeleafDialect extends AbstractDialect implements IExpressionObjectDialect {

    public static final String NAME = "Perun";


    protected PerunThymeleafDialect(){
        super(NAME);
    }

    @Override
    public IExpressionObjectFactory getExpressionObjectFactory(){
        return null;
    }
}
